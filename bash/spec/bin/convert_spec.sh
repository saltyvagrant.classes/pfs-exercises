Describe 'convert'
  Parameters
    "#1" "" "Hello !"
    "#2" "Mark" "Hello Mark!"
  End
  It "shows greeting $1"
    When run script bin/convert "$2"
    The output should equal "$3"
  End
End
