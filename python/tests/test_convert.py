import pytest
import os

@pytest.mark.parametrize("name,expected", [ ("", "Hello !\n"), ("Mark", "Hello Mark!\n")])
def test_welcome_message(name, expected, capfd):
    os.system(f"poetry run python convert.py \"{name}\"")
    captured = capfd.readouterr()
    assert captured.out == expected
